
Simplex is a linear programming / integer linear programming solver written in python.  It uses the simplex algorithm (duh), with Gomory/Chvatal cutting planes for integer lp's.

* Only dependency is numpy.

* Usage:
* LP must be maximization in standard form
*     *  all equations must be in the form a1x1 + a2x2 ... anxn <= b1 etc., objective function must be a maximization.
*     *  x1, x2...xn >= 0

```
#!python

(for LP)
from simplex import Simplex
s = Simplex(b, A, c)
result = s.get_result()
coef_dict = s.get_result_dict()

(for ILP)
s = Simplex(b, A, c, ILP=True)
```
* 
* 
* LP example:
* maximize -2x1 - x2
* s.t.
* -x1 + x2 <= 1
* -x1 + -x2 <= 2
* x1 - 2x2 <= 4
* 

```
#!python

import numpy as np
from simplex import Simplex
A = np.array([[-1, 1], [-1, -1], [1, -2]])
b = np.array([1, 2, 4])
c = np.array([-2, -1])
s = Simplex(b, A, c)
s.get_result()
s.get_result_dict()

```


Released under the MIT license.  Please feel free to use/modify/improve code as you see fit.