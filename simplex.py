#!/usr/bin/python


#inputs must match standard form for lp (Ax <= b, max c T x),
#length b == M, length c coeficient array == N
#If ILP, coeficients must be scaled to integers
#ILP solver uses Gomory/Chvatal cutting planes

#Example usage for ILP (omit ILP=True if not)
#original problem: |4 7|	|21|
#	      A =  |0 1| x <=   | 9|
#		   |1 0|  	| 3|
#
#	maximize 2x1 + x2

#from simplex import Simplex
#A = np.array([[4, 7], [0, 1], [1, 0]])
#b = np.array([21, 9, 3])
#obj_coef = np.array([2, 1])
#s = Simplex(b, A, obj_coef, ILP=True)
#s.get_result() 	-- prints objective value (or INFEASIBLE / UNBOUNDED)
#s.get_result_dict() 	-- returns dictionary of coeficient values 
# 7  {1: 3, 2: 1}	-- (obj. value 7, x1 = 3, x2 = 1)

import sys
import copy
import numpy as np
import math

class Simplex:
	
	#Adjust EPSILON to account for floating-point errors
	EPSILON = 1.e-9
	
	def __init__(self, b, A, c, **kwargs):
		if (kwargs.get('ILP')):
			is_ILP = kwargs['ILP']
		else:
			is_ILP = False
		self.__check_input(b, A, c)
		self.b = copy.deepcopy(b)
		self.A = copy.deepcopy(A)
		self.A *= -1
		self.obj_coef = np.insert(c, 0, 0)
		self.M = len(A)
		self.N = len(A[0])
		self.bases = [x + self.N + 1 for x in range(self.M)]
		self.non_bases = [x + 1 for x in range(self.N)]
		self.is_dual = False
		self.is_feasible = True
		if (not all(i >= 0 for i in self.b)):
			self.__initialize()
		self.is_bounded = self.__optimize()
		if (is_ILP and self.is_feasible and self.is_bounded):
			self.is_feasible = self.__do_ILP()
			
	def __check_input(self, b, A, obj_coef):
		if (len(b) != len(A)):
			raise Exception ('Input dimension mismatch, b != M')
		if (len(obj_coef) != len(A[0])):
			raise Exception ('Input dimension mismatch, obj_coef != N')
		
	def __initialize(self):
		old_coef = copy.deepcopy(self.obj_coef)
		old_non_basic = [i for i in self.non_bases]
		self.obj_coef = -1 * np.ones(len(old_coef))
		self.__dualize()
		self.is_feasible = self.__optimize()
		self.__dualize()
		self.obj_coef = np.zeros(len(old_coef))
		for i in range(len(old_non_basic)):
			if (old_non_basic[i] in self.non_bases):
				self.obj_coef[self.non_bases.index(old_non_basic[i]) + 1] += old_coef[i + 1]
			else:
				l_loc = self.bases.index(old_non_basic[i])
				self.obj_coef += (np.insert(self.A[l_loc], 0, self.b[l_loc]).ravel()) * old_coef[i + 1]
		
	def __dualize(self):
		temp = self.M
		self.M = self.N
		self.N = temp
		self.A = -1 * self.A.transpose()
		self.A[self.A == -0] = 0
		temp = [i for i in self.non_bases]
		self.non_bases = [i for i in self.bases]
		self.bases = [i for i in temp]
		temp = copy.deepcopy(self.b)
		self.b = -1 * self.obj_coef[1:]
		self.b[self.b == -0] = 0
		self.obj_coef = -1 * np.insert(temp, 0, self.obj_coef[0]).ravel()
		self.obj_coef[self.obj_coef == -0] = 0
		self.is_dual = not self.is_dual
	
	def __optimize(self):
		while True:
			entering = self.__find_entering()
			if (entering == -1):
				return True
			leaving = self.__find_leaving(entering)
			if (leaving == -1):
				return False
			self.__pivot(entering, leaving)	

	def __find_entering(self):
		enter_cand = sorted([self.non_bases[i] for i in range(self.N) if (self.obj_coef[i + 1] > Simplex.EPSILON)])
		return enter_cand[0] if (len(enter_cand) > 0) else -1

	def __find_leaving(self, e_var):
		e_var_loc = self.non_bases.index(e_var)
		leaving_cand = [i for i in range(self.M) if (self.A[i, e_var_loc] < -Simplex.EPSILON)]
		if (len(leaving_cand) < 1):
			return -1
		elif (len(leaving_cand) == 1):
			return self.bases[leaving_cand[0]]
		leav_values = [-1 * self.b[cand] / self.A[cand, e_var_loc] for cand in leaving_cand]
		i = -1
		for j in range(len(leav_values)):
			if (i == -1 or leav_values[j] < leav_values[i[0]]):
				i = [j]
			elif leav_values[j] == leav_values[i[0]]:
				i.append(j)
		return sorted([self.bases[leaving_cand[j]] for j in i])[0]

	def __pivot(self, entering, leaving):
		e_loc = self.non_bases.index(entering)
		l_loc = self.bases.index(leaving)
		div_factor = -float(self.A[l_loc][e_loc])
		self.A[l_loc][e_loc] = -1.
		self.A[l_loc] /= div_factor
		self.b[l_loc] /= div_factor
		trans_vec = np.insert(self.A[l_loc], 0, self.b[l_loc])
		for i in range(len(self.bases)):
			if (i != l_loc):
				temp_trans = np.array(trans_vec * self.A[i][e_loc])
				self.A[i][e_loc] = 0
				self.b[i] += temp_trans[0]
				self.A[i] = np.array([self.A[i][j] + temp_trans[j + 1] for j in range(len(self.A[i]))])
		self.bases[l_loc] = entering
		self.non_bases[e_loc] = leaving
		temp_trans = np.array(trans_vec * self.obj_coef[e_loc + 1])
		self.obj_coef[e_loc + 1] = 0
		self.obj_coef += temp_trans

	def __do_ILP(self):
		while True:
			non_ints = self.__check_b()
			if (len(non_ints) < 1):
				return True
			self.__add_cutting_planes(non_ints)
			self.__dualize()
			if not self.__optimize():
				return False
			self.__dualize()
			
	def __add_cutting_planes(self, non_ints):
		for row in non_ints:
			self.M += 1
			self.bases.append(self.N + self.M)
			self.b = np.append(self.b, -self.__get_frac(-self.b[row]))
			new_row = [self.__get_frac(x) for x in self.A[row]]
			self.A = np.vstack([self.A, new_row])
			
	def __get_frac(self, x):
		return (-x - math.floor(-x))
		
	def __check_b(self):
		return [x for x in range(len(self.b)) if not self.__check_int(self.b[x])]
		
	def __check_int(self, x):
		return (abs(x - round(x)) < Simplex.EPSILON)
	
	def get_result(self):
		if not self.is_feasible:
			return 'INFEASIBLE'
		elif not self.is_bounded:
			return 'UNBOUNDED'
		else:
			return self.obj_coef[0]

	def save_results(self, file_name):
		file_output = '%.3f' % (self.get_result()) if (self.is_feasible and self.is_bounded) else self.get_result()
		with open(file_name, 'w') as f:
			f.write(file_output)
		return

	def print_results(self):
		print '%.3f' % (self.get_result()) if (self.is_feasible and self.is_bounded) else self.get_result()
		return

	def get_result_dict(self):
		result = dict(zip(self.bases, self.b))
		for key in self.non_bases:
			result[key] = 0
		return {key:result[key] for key in result if key <= self.N}
		
	def print_dict(self):
		for i in range(len(self.bases)):
			print '\n%d | %.3f ' % (self.bases[i], self.b[i]),
			for j in range(len(self.non_bases)):
				print ' %.3f ' % (self.A[i][j]),
		print '\n------------------'
		print 'z | %s\n' % (self.obj_coef)


def parse_data(file_name):
	with open(file_name, 'r') as f:
		file_data = f.read()
	lines = file_data.splitlines()
	[M, N] = [int(i) for i in lines[0].split(' ') if (i != '')]
	bases = [int(i) for i in lines[1].split(' ') if (i != '')]
	non_bases = [int(i) for i in lines[2].split(' ') if (i != '')]
	b = np.array([float(i) for i in lines[3].split(' ') if (i != '')])
	A = np.array([[float(i) for i in lines[4 + j].split(' ') if (i != '')] for j in range(M)])
	obj_coef = np.array([float(i) for i in lines[4 + M].split(' ') if (i != '')])
	return (M, N, bases, non_bases, b, A, obj_coef)

if __name__=="__main__":
	if (len(sys.argv) < 2):
		print 'please enter name of input dictionary'
		sys.exit()
	file_name = sys.argv[1]
	(M, N, bases, non_bases, b, A, obj_coef) = parse_data(file_name)
	obj_coef = obj_coef[1:]
	simplex = Simplex(b, A, obj_coef, ILP=True)
	simplex.print_results()
	simplex.save_results(file_name + '_out')
	print simplex.get_result_dict()